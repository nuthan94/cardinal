package pom;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.BasePageCardinal;

public class ForgotPassword extends BasePageCardinal
{
	//Verify your account page
	
	@FindBy(xpath="//h3[.='Verify your account']")
	private WebElement verifyAccountPage;
	
	@FindBy(xpath="//input[@placeholder='Employee Number']")
	private WebElement employeeNumber;
	
	@FindBy(xpath="//input[@placeholder='DD']")
	private WebElement DD;
	
	@FindBy(xpath="//input[@placeholder='MM']")
	private WebElement MM;
	
	@FindBy(xpath="//input[@placeholder='YYYY']")
	private WebElement YYYY;
	
	@FindBy(xpath="/html/body/app-dashboard/ng-component/div/div/div/div/div/div[2]/div/div/div[2]/label")
	private WebElement DOB;
	
	@FindBy(xpath="//button[.='Verify']")
	private WebElement verifyBTN;
	
	@FindBy(xpath="//button[.='Cancel']")
	private WebElement cancelBTN;
	
	@FindBy(xpath="//*[@id='swal2-content']")
	private WebElement verifyAccountERROR;
	
	@FindBy(xpath="//button[.='OK']")
	private WebElement errorOK;
	
	//Reset password page
	
	@FindBy(xpath="//h3[.='Reset password']")
	private WebElement ResetPasswordPage;
	
	@FindBy(xpath="/html/body/app-dashboard/ng-component/div/div/div/div/div/div[2]/div/div/p[1]")
	private WebElement NewPassword;
	
	@FindBy(xpath="/html/body/app-dashboard/ng-component/div/div/div/div/div/div[2]/div/div/p[2]")
	private WebElement ConfirmPassword;
	
	@FindBy(xpath="(//input[@placeholder='DD'])[1]")
	private WebElement NewPasswordDD;
	
	@FindBy(xpath="(//input[@placeholder='MM'])[1]")
	private WebElement NewPasswordMM;
	
	@FindBy(xpath="(//input[@placeholder='YYYY'])[1]")
	private WebElement NewPasswordYYYY;
	
	@FindBy(xpath="(//input[@placeholder='DD'])[2]")
	private WebElement ConfirmPasswordDD;
	
	@FindBy(xpath="(//input[@placeholder='MM'])[2]")
	private WebElement ConfirmPasswordMM;
	
	@FindBy(xpath="(//input[@placeholder='YYYY'])[2]")
	private WebElement ConfirmPasswordYYYY;
	
	@FindBy(xpath="//button[.='Submit']")
	private WebElement SubmitBTN;
	
	@FindBy(xpath="//div[contains(text(),'Password')]")
	private WebElement PasswordDoesntMatch;
	
	@FindBy(xpath="//div[.='Updated Successfully']")
	private WebElement UpdatedMSG;
	
	
	public ForgotPassword(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}

	WebDriverWait wait=new WebDriverWait(driver, 10);

	
	public void verifyaccountPage() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(verifyAccountPage));
			assertEquals(verifyAccountPage.getText(), "Verify your account");
			Reporter.log("Page: "+verifyAccountPage.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("Page name is not matching",true);
			Assert.fail();
		}
	}

	public void verifyEmployeeNumber() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(employeeNumber));
			Reporter.log("Employee Number",true);
		}
		catch(Exception e)
		{
			Reporter.log("employeeNumber not present",true);
			Assert.fail();
		}
	}

	public void verifyDOB() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(DOB));
			Reporter.log(DOB.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("DOB not present",true);
			Assert.fail();
		}
		
	}

	public void verifyButton() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(verifyBTN));
			Reporter.log(verifyBTN.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("verifyBTN not present",true);
			Assert.fail();
		}
	}

	public void verifyCancelBTN() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(cancelBTN));
			Reporter.log(cancelBTN.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("cancelBTN not present",true);
			Assert.fail();
		}
	}

	public void setEmployeeNumber(String Enumber) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(employeeNumber));
			employeeNumber.clear();
			employeeNumber.sendKeys(Enumber);
			Reporter.log("E.number : "+Enumber,true);
		}
		catch(Exception e)
		{
			Reporter.log("employeeNumber not present",true);
			Assert.fail();
		}
		
	}

	public void setDD(String date)
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(DD));
			DD.clear();
			DD.sendKeys(date);
			Reporter.log("DD :"+date,true);
		}
		catch(Exception e)
		{
			Reporter.log("DD not present",true);
			Assert.fail();
		}
	}

	public void setMM(String month) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(MM));
			MM.clear();
			MM.sendKeys(month);
			Reporter.log("MM :"+month,true);
		}
		catch(Exception e)
		{
			Reporter.log("MM not present",true);
			Assert.fail();
		}
	}

	public void setYYYY(String year) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(YYYY));
			YYYY.clear();
			YYYY.sendKeys(year);
			Reporter.log("YYYY :"+year,true);
		}
		catch(Exception e)
		{
			Reporter.log("YYYY not present",true);
			Assert.fail();
		}
	}

	public void clickVerify() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(verifyBTN));
			verifyBTN.click();
		}
		catch(Exception e)
		{
			Reporter.log("verifyBTN not present",true);
			Assert.fail();
		}
	}

	public void verifyResetPasswordPage()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ResetPasswordPage));
			Assert.assertEquals(ResetPasswordPage.getText(), "Reset password");
			Reporter.log("Page : "+ResetPasswordPage.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("ResetPasswordPage not present",true);
			Assert.fail();
		}		
	}

	public void verifyNewPassword()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(NewPassword));
			Reporter.log(NewPassword.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("NewPassword not present",true);
			Assert.fail();
		}
	}

	public void verifyConfirmPassword() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ConfirmPassword));
			Reporter.log(ConfirmPassword.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("ConfirmPassword not present",true);
			Assert.fail();
		}
	}

	public void verifyNewPasswordDD() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(NewPasswordDD));
			Reporter.log("New Password Date Placeholder text : "+NewPasswordDD.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("NewPasswordDD not present",true);
			Assert.fail();
		}
	}

	public void verifyNewPasswordMM() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(NewPasswordMM));
			Reporter.log("New Password Month Placeholder text : "+NewPasswordMM.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("NewPasswordMM not present",true);
			Assert.fail();
		}
	}

	public void verifyNewPasswordYYYY() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(NewPasswordYYYY));
			Reporter.log("New Password Year Placeholder text : "+NewPasswordYYYY.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("NewPasswordYYYY not present",true);
			Assert.fail();
		}
	}

	public void verifyConfirmPasswordDD()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ConfirmPasswordDD));
			Reporter.log("Confirm Password Date Placeholder text : "+ConfirmPasswordDD.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("ConfirmPasswordDD not present",true);
			Assert.fail();
		}
	}

	public void verifyConfirmPasswordMM()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ConfirmPasswordMM));
			Reporter.log("Confirm Password Month Placeholder text : "+ConfirmPasswordMM.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("ConfirmPasswordMM not present",true);
			Assert.fail();
		}
	}

	public void verifyConfirmPasswordYYYY() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ConfirmPasswordYYYY));
			Reporter.log("Confirm Password Year Placeholder text : "+ConfirmPasswordYYYY.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("ConfirmPasswordYYYY not present",true);
			Assert.fail();
		}
	}

	public void clickCancel()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(cancelBTN));
			cancelBTN.click();
		}
		catch(Exception e)
		{
			Reporter.log("cancelBTN not present",true);
			Assert.fail();
		}
	}

	public void errorMSG() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(verifyAccountERROR));
			Reporter.log("ERROR MESSAGE : "+verifyAccountERROR.getText(),true);
			Thread.sleep(5000);
			errorOK.click();
			
		}
		catch(Exception e)
		{
			Reporter.log("verifyAccountERROR not present",true);
			Assert.fail();
		}
	}

	public void checkVerifyDisable() 
	{
		try{
			wait.until(ExpectedConditions.elementToBeClickable(verifyBTN));
			Reporter.log("Verify button is disabled",true);
		}
		catch(Exception e)
		{
			Reporter.log("verifyBTN is disabled",true);
			Assert.fail();
		}
	}

	public void setNewPasswordDD(String DD) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(NewPasswordDD));
			NewPasswordDD.clear();
			NewPasswordDD.sendKeys(DD);
			Reporter.log("New DD :"+DD,true);
		}
		catch(Exception e)
		{
			Reporter.log("NewPasswordDD is not present",true);
			Assert.fail();
		}
	}

	public void setNewPasswordMM(String MM) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(NewPasswordMM));
			NewPasswordMM.clear();
			NewPasswordMM.sendKeys(MM);
			Reporter.log("New MM :"+MM,true);
		}
		catch(Exception e)
		{
			Reporter.log("NewPasswordMM is not present",true);
			Assert.fail();
		}
	}

	public void setNewPasswordYYYY(String YYYY) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(NewPasswordYYYY));
			NewPasswordYYYY.clear();
			NewPasswordYYYY.sendKeys(YYYY);
			Reporter.log("New YYYY :"+YYYY,true);
		}
		catch(Exception e)
		{
			Reporter.log("NewPasswordYYYY is not present",true);
			Assert.fail();
		}
	}

	public void clickSubmit() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(SubmitBTN));
			SubmitBTN.click();
		}
		catch(Exception e)
		{
			Reporter.log("SubmitBTN is not present",true);
			Assert.fail();
		}
	}

	public void setConfirmPasswordDD(String DD)
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ConfirmPasswordDD));
			ConfirmPasswordDD.clear();
			ConfirmPasswordDD.sendKeys(DD);
			Reporter.log("Confirm DD :"+DD,true);
		}
		catch(Exception e)
		{
			Reporter.log("ConfirmPasswordDD is not present",true);
			Assert.fail();
		}
	}

	public void setConfirmPasswordMM(String MM) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ConfirmPasswordMM));
			ConfirmPasswordMM.clear();
			ConfirmPasswordMM.sendKeys(MM);
			Reporter.log("Confirm MM :"+MM,true);
		}
		catch(Exception e)
		{
			Reporter.log("ConfirmPasswordMM is not present",true);
			Assert.fail();
		}
	}

	public void setConfirmPasswordYYYY(String YYYY) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ConfirmPasswordYYYY));
			ConfirmPasswordYYYY.clear();
			ConfirmPasswordYYYY.sendKeys(YYYY);
			Reporter.log("Confirm YYYY :"+YYYY,true);
		}
		catch(Exception e)
		{
			Reporter.log("ConfirmPasswordYYYY is not present",true);
			Assert.fail();
		}
	}

	public void verifyPasswordDoesntMatch()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(PasswordDoesntMatch));
			Reporter.log("ERROR MESSAGE : "+PasswordDoesntMatch.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("PasswordDoesntMatch is not present",true);
			Assert.fail();
		}
	}

	public void verifySubmit()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(SubmitBTN));
			Reporter.log(SubmitBTN.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("SubmitBTN not present",true);
			Assert.fail();
		}
	}

	public void verifyMSG() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(UpdatedMSG));
			Reporter.log(UpdatedMSG.getText(),true);
			errorOK.click();
		}
		catch(Exception e)
		{
			Reporter.log("Updated message is not displayed",true);
			Assert.fail();
		}
		
	}
	

}