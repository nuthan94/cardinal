package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.BasePageCardinal;

public class StoreCardinal extends BasePageCardinal
{

	public StoreCardinal(WebDriver driver)
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//button[.='Store']")
	private WebElement storeTAB;	
	
	@FindBy(xpath="//button[@class='btn btn-secondary cursorHand firstrightButton']")
	private WebElement addStore;
	
	@FindBy(xpath="//button[@touranchor='master.export']")
	private WebElement storeExport;
	
	@FindBy(xpath="//*[@id='storenum1']")
	private WebElement addStoreNum;
	
	@FindBy(xpath="//*[@id='store1']")
	private WebElement addStoreName;
	
	@FindBy(xpath="//*[@id='address1']")
	private WebElement addStoreAddress;
	
	@FindBy(xpath="//*[@id='radius']")
	private WebElement addStoreRadius;
	
	@FindBy(xpath="//*[@id='lat']")
	private WebElement addStoreLatitude;
	
	@FindBy(xpath="//*[@id='long']")
	private WebElement addStoreLongitude;
	
	@FindBy(xpath="//button[.='Cancel']")
	private WebElement addStoreCancel;
	
	@FindBy(xpath="//button[.='Submit']")
	private WebElement addStoreSubmit;
	
	@FindBy(xpath="//th[.='Store Name']")
	private WebElement storeName;
	
	@FindBy(xpath="//th[.='Address']")
	private WebElement storeAddress;
	
	@FindBy(xpath="//th[.='Set Store']")
	private WebElement storeSetStore;
	
	@FindBy(xpath="//th[.='Status']")
	private WebElement storeStatus;
	
	@FindBy(xpath="//th[.='Employees']")
	private WebElement storeEmployees;
	
	@FindBy(xpath="//th[.='Edit']")
	private WebElement storeEdit;
	
	@FindBy(xpath="//input[@placeholder='Store Number or Store Name or Status']")
	private WebElement storeSearch;

	
	WebDriverWait wait=new WebDriverWait(driver, 20);

	public void clickStoreTab() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(storeTAB));
		storeTAB.click();
		}
		catch(Exception e)
		{
			Reporter.log("storeTAB is not displayed",true);
			Assert.fail();
		}
	}

	public void clickAddStore() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(addStore));
		addStore.click();
		}
		catch(Exception e)
		{
			Reporter.log("addStore is not displayed",true);
			Assert.fail();
		}
	}

	public void enterStoreNumber(String storeNumber) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(addStoreNum));
		addStoreNum.sendKeys(storeNumber);
		}
		catch(Exception e)
		{
			Reporter.log("addStoreNum is not displayed",true);
			Assert.fail();
		}
	}

	public void enterStoreName(String storeName) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(addStoreName));
		addStoreName.sendKeys(storeName);	
		}
		catch(Exception e)
		{
			Reporter.log("addStoreName is not displayed",true);
			Assert.fail();
		}
	}

	public void enterStoreAddress(String storeAddress) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(addStoreAddress));
		addStoreAddress.sendKeys(storeAddress);
		}
		catch(Exception e)
		{
			Reporter.log("addStoreAddress is not displayed",true);
			Assert.fail();
		}
	}

	public void enterStoreRadius(String storeRadius) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(addStoreRadius));
		addStoreRadius.sendKeys(storeRadius);
		}
		catch(Exception e)
		{
			Reporter.log("addStoreRadius is not displayed",true);
			Assert.fail();
		}
	}

	public void enterStoreLatitude(String storeLatitude) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(addStoreLatitude));
		addStoreLatitude.sendKeys(storeLatitude);
		}
		catch(Exception e)
		{
			Reporter.log("addStoreLatitude is not displayed",true);
			Assert.fail();
		}
	}

	public void enterStoreLongitude(String storeLongitude) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(addStoreLongitude));
		addStoreLongitude.sendKeys(storeLongitude);
		}
		catch(Exception e)
		{
			Reporter.log("addStoreLongitude is not displayed",true);
			Assert.fail();
		}
	}

	public void clickStoreSubmit() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(addStoreSubmit));
		addStoreSubmit.click();
		}
		catch(Exception e)
		{
			Reporter.log("addStoreSubmit is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyAddStore() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(addStore));
			Reporter.log(addStore.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("addStore is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyStoreExport()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(storeExport));
			Reporter.log(storeExport.getText(),true);
			}
			catch(Exception e)
			{
				Reporter.log("storeExport is not displayed",true);
				Assert.fail();
			}
	}

	public void verifyStoreSearch() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(storeSearch));
			Reporter.log(storeSearch.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("storeSearch is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyStoreName() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(storeName));
			Reporter.log(storeName.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("storeName is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyStoreAddress() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(storeAddress));
			Reporter.log(storeAddress.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("storeAddress is not displayed",true);
			Assert.fail();
		}
	}

	public void verifySetStore() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(storeSetStore));
			Reporter.log(storeSetStore.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("storeSetStore is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyStoreStatus() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(storeStatus));
			Reporter.log(storeStatus.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("storeStatus is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyStoreEmployees()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(storeEmployees));
			Reporter.log(storeEmployees.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("storeEmployees is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyStoreEdit() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(storeEdit));
			Reporter.log(storeEdit.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("storeEdit is not displayed",true);
			Assert.fail();
		}
	}

}