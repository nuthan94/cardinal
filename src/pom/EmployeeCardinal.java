package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.BasePageCardinal;

public class EmployeeCardinal extends BasePageCardinal
{

	public EmployeeCardinal(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(xpath="//button[@touranchor='master.addEmployee']")
	private WebElement addEmployee;	
	
	@FindBy(xpath="//span[.='E Number *']")
	private WebElement vENumber;
	
	@FindBy(xpath="//span[.='Reg Number']")
	private WebElement vRegNumber;
	
	@FindBy(xpath="//span[.='Employee Type']")
	private WebElement vEmpType	;
	
	@FindBy(xpath="//span[.='Name *']")
	private WebElement vName;
	
	@FindBy(xpath="//span[.='Gender']")
	private WebElement vGender;
	
	@FindBy(xpath="//span[.='Location']")
	private WebElement vLocation;
	
	@FindBy(xpath="//span[.='Role']")
	private WebElement vRole;
	
	@FindBy(xpath="//span[.='Designation']")
	private WebElement vDesignation;
	
	@FindBy(xpath="//div[@class='input-group-addon empExtend multiClass']")
	private WebElement vDepartment;
	
	@FindBy(xpath="//span[.='Date of Birth *']")
	private WebElement vDOB;
	
	@FindBy(xpath="//span[.='Store']")
	private WebElement vStore;
	
	@FindBy(xpath="//button[.='Submit']")
	private WebElement Submit;
	
	@FindBy(xpath="//button[.='Cancel']")
	private WebElement Cancel;
	
	@FindBy(xpath="//input[@name='emp3']")
	private WebElement enumber;
	
	@FindBy(xpath="//input[@name='reg3']")
	private WebElement regNumber;
	
	@FindBy(xpath="//*[@id='select1']")
	private WebElement EmpType;
	
	@FindBy(xpath="//*[@name='name']")
	private WebElement name;
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[5]/div/select")
	private WebElement Gender;
	
	@FindBy(xpath="//*[@name='loc5']")
	private WebElement 	Location;
	
	@FindBy(xpath="//*[@id='select']")
	private WebElement Role;
	
	@FindBy(xpath="//*[@id='select3']")
	private WebElement Designation;
																																							
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[1]/div")
	private WebElement Department;
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/div[1]")
	private WebElement UnselectAllDept;	
	
	@FindBy(xpath="//input[@placeholder='DD/MM/YYYY']")
	private WebElement DOB;
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[10]/div/div/input")
	private WebElement Store;
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[10]/div/div/span[2]/button")
	private WebElement Add_Store_BTN;

	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[10]/div[2]/div[1]/div[2]")
	private WebElement storeSearch1;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[1]/div/div/div[4]/table/tbody/tr/td[2]/label/input")
	private WebElement Search_Delete1;
	
	@FindBy(xpath="/html/body/div/div/div[10]/button[1]")
	private WebElement DeleteNo;
	
	@FindBy(xpath="/html/body/div/div/div[10]/button[2]")
	private WebElement DeleteYes;
	
	@FindBy(xpath="/html/body/div[2]/div/div[10]/button[1]")
	private WebElement deleteOK;
	
	@FindBy(xpath="/html/body/div/div/div[10]/button[1]")
	private WebElement EmpCreationOK;
	
	@FindBy(xpath="//*[@id='appendedInputButton']")
	private WebElement employeeSearchTEXT;		//Employee search
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[1]/div/div/div[1]/div/div[2]/div/span/button")
	private WebElement employeeSearchBTN;		//Employee search button
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[1]/div/div/div[1]/div/div[2]/div/span[1]")
	private WebElement employeeSearchCancel;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[2]/div/div/div[2]/table/thead/tr/th[2]/label/input")
	private WebElement DeleteAllCHECKBOX;	//Delete all checkbox
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[2]/div/div/div[3]/table/tbody/tr[1]/td[2]/label/input")
	private WebElement checkbox1;			//checkbox 1
	
	@FindBy(xpath="//*[@id='swal2-content']")
	private WebElement empcreationmsg;
	
	@FindBy(xpath="//div[.='Employee number already exists']")
	private WebElement empAlreadyExistERROR_MSG;
	
	@FindBy(xpath="/html/body/div[2]/div/div[10]/button[1]")
	private WebElement empAlreadyExistERROR_MSG_OK;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[1]/div[2]/div[1]/div/div/div[2]/div/i")
	private WebElement empSearchCancel;
	
	@FindBy(xpath="//span[.='Employee']")
	private WebElement EmployeeText;
	
	@FindBy(xpath="//input[@placeholder='Employee Number or Name or Status']")
	private WebElement EmployeeSearch;
		
	WebDriverWait wait=new WebDriverWait(driver, 10);

	public void clickAddEmployee() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(addEmployee));
			addEmployee.click();
		}
		catch(Exception e)
		{
			Reporter.log("addEmployee is not present",true);
			Assert.fail();	
		}
	}

	public void verifyEnumber() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vENumber));
			Reporter.log(vENumber.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vENumber is not present",true);
			Assert.fail();	
		}
	}

	public void verifyRegNumber() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vRegNumber));
			Reporter.log(vRegNumber.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vRegNumber is not present",true);
			Assert.fail();	
		}
	}

	public void verifyEmpType()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vEmpType));
			Reporter.log(vEmpType.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vEmpType is not present",true);
			Assert.fail();	
		}
	}

	public void verifyName()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vName));
			Reporter.log(vName.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vName is not present",true);
			Assert.fail();	
		}
	}

	public void verifyGender() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vGender));
			Reporter.log(vGender.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vGender is not present",true);
			Assert.fail();	
		}
	}

	public void verifyLocation()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vLocation));
			Reporter.log(vLocation.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vLocation is not present",true);
			Assert.fail();	
		}
	}

	public void verifyRole()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vRole));
			Reporter.log(vRole.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vRole is not present",true);
			Assert.fail();	
		}
	}

	public void verifyDesignation()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vDesignation));
			Reporter.log(vDesignation.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vDesignation is not present",true);
			Assert.fail();	
		}
	}

	public void verifyDepartment() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vDepartment));
			Reporter.log(vDepartment.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vDepartment is not present",true);
			Assert.fail();	
		}
	}

	public void verifyDOB() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vDOB));
			Reporter.log(vDOB.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vDOB is not present",true);
			Assert.fail();	
		}
	}

	public void verifyStore() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(vStore));
			Reporter.log(vStore.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("vStore is not present",true);
			Assert.fail();	
		}
	}

	public void verifySubmit() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(Submit));
			Reporter.log(Submit.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("Submit is not present",true);
			Assert.fail();	
		}
	}

	public void verifyCancel()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(Cancel));
			Reporter.log(Cancel.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("Cancel is not present",true);
			Assert.fail();	
		}
	}

	public void clickCancel() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(Cancel));
			Cancel.click();
		}
		catch(Exception e)
		{
			Reporter.log("Cancel is not present",true);
			Assert.fail();	
		}
	}
	
	public void setEnumber(String Enumb) throws InterruptedException 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(enumber));
			enumber.sendKeys(Enumb);
		}
		catch(Exception e)
		{
			Reporter.log("Enumb not displayed",true);
			Assert.fail();
		}
	}
	
	public void setRegNumber(String RegNum) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(regNumber));
			regNumber.sendKeys(RegNum);
		}
		catch(Exception e){
			Reporter.log("Add_RegNum not displayed",true);
			Assert.fail();
		}
	}
	
	public void selectEmployeeType(String employeeType) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EmpType));
		
			Select select=new Select(EmpType);
			select.selectByVisibleText(employeeType);
		}
		catch (Exception e) 
		{
			Reporter.log("EmpType not displayed",true);
			Assert.fail();
		}
	}
	
	public void setEname(String Ename) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(name));
			name.sendKeys(Ename);
		}
		catch (Exception e) 
		{
			Reporter.log("name not displayed",true);
			Assert.fail();
		}
	}
	
	public void selectGender(String EGender) 
	{		
		try{
			wait.until(ExpectedConditions.visibilityOf(Gender));
			Select select=new Select(Gender);
				if(EGender=="Male")
				{
					select.selectByValue("Male");
				}
			else if (EGender=="Female") 
				{
					select.selectByValue("Female");
				}
			}
		catch (Exception e)
		{
			Reporter.log("Gender not displayed",true);
			Assert.fail();
		}
	}
}
