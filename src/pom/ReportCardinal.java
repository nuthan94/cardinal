package pom;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import generic.BasePageCardinal;

public class ReportCardinal extends BasePageCardinal
{
	public ReportCardinal(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//a[@href='#/report']")
	private WebElement reportTAB;

	@FindBy(xpath="//a[@href='#/imports']")
	private WebElement importTAB;
	
	@FindBy(xpath="//button[.='Today']")
	private WebElement Today;
	
	@FindBy(xpath="//button[@touranchor='report.yesterday']")
	private WebElement Yesterday;
	
	@FindBy(xpath="//button[@touranchor='report.lastweek']")
	private WebElement Last7Days;
	
	@FindBy(xpath="//button[@touranchor='report.lastmonth']")
	private WebElement Last30Days;
	
	@FindBy(xpath="//button[@class='btn btn-secondary hoverProp firstrightButton']")
	private WebElement ShowAll;
	
	@FindBy(xpath="//button[@touranchor='report.filter']")
	private WebElement Filter;
	
	@FindBy(xpath="//button[.='Take a Tour']")
	private WebElement TakeATourReport;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[1]/div[2]/div[1]/div/div/div[2]/div/input")
	private WebElement EmployeeSearch;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[1]/div[2]/div[1]/div/div/div[2]/div/span")
	private WebElement EmployeeSearchBTN;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[1]/div[2]/div[2]/div[1]/div[1]/div/div[1]/span")
	private WebElement ExpandEmp;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[1]/div[2]/div[2]/div[1]/div[2]/div/div/div/div[1]/div[1]/div/div[2]")
	private WebElement ExpEmpdata;
	
	//Filter
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[4]/div/div/div[2]/div[1]/div/div/div")
	private WebElement FilterDept;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[4]/div/div/div[2]/div[2]/div/div/div")
	private WebElement FilterLoc;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[4]/div/div/div[2]/div[3]/div[1]/label")
	private WebElement FilterStartDate;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[4]/div/div/div[2]/div[4]/div[1]/label")
	private WebElement FilterEndDate;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[4]/div/div/div[3]/button[1]")
	private WebElement FilterReset;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[4]/div/div/div[3]/button[2]")
	private WebElement FilterApply;
	
	WebDriverWait wait=new WebDriverWait(driver, 10);

	public void verifyReport() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(reportTAB));
		Reporter.log(reportTAB.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("Report button is not present",true);
			Assert.fail();
		}
	}

	public void verifyToday() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Today));
		Reporter.log(Today.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Today button is not present",true);
			Assert.fail();
		}
	}

	public void verifyYesterday() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Yesterday));
		Reporter.log(Yesterday.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Yesterday button is not present",true);
			Assert.fail();
		}
	}

	public void verifyLast7Days() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Last7Days));
		Reporter.log(Last7Days.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Week button is not present",true);
			Assert.fail();
		}
	}

	public void verifyLast30Days() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Last30Days));
		Reporter.log(Last30Days.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Month button is not present",true);
			Assert.fail();
		}
		
	}

	public void clickReport() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(reportTAB));
		reportTAB.click();
		}
		catch(Exception e)
		{
			Reporter.log("Report button is not present",true);
			Assert.fail();
		}
	}

	public void verifyShowAll() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(ShowAll));
		Reporter.log(ShowAll.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Show all button is not present",true);
			Assert.fail();
		}
	}

	public void verifyFilter() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Filter));
		Reporter.log(Filter.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Filter button is not present",true);
			Assert.fail();
		}
	}

	public void verifyTakeATour() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(TakeATourReport));
		Reporter.log(TakeATourReport.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Take a tour button is not present",true);
			Assert.fail();
		}
	}

	public void verifyEmployeeSearch() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(EmployeeSearch));
		Reporter.log(EmployeeSearch.getAttribute("placeholder"),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Employee search button is not present",true);
			Assert.fail();
		}
	}

	public void clickToday() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Today));
		Today.click();
		}
		catch(Exception e)
		{
			Reporter.log("Today button is not present",true);
			Assert.fail();
		}
	}

	public void clickYesterday() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Yesterday));
		Yesterday.click();
		}
		catch(Exception e)
		{
			Reporter.log("Yesterday button is not present",true);
			Assert.fail();
		}
	}

	public void click7Days() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Last7Days));
		Last7Days.click();
		}
		catch(Exception e)
		{
			Reporter.log("Week button is not present",true);
			Assert.fail();
		}
	}

	public void clickMonth() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Last30Days));
		Last30Days.click();		
		}
		catch(Exception e)
		{
			Reporter.log("Month button is not present",true);
			Assert.fail();
		}
	}

	public void clickEmpExpand()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ExpandEmp));
			ExpandEmp.click();
		}
		catch(Exception e)
		{
			Reporter.log("Expand Employee button is not present",true);
			Assert.fail();
		}
	}

	public void clickExpData() {
		try{
			wait.until(ExpectedConditions.visibilityOf(ExpEmpdata));
			ExpEmpdata.click();
		}
		catch(Exception e)
		{
			Reporter.log("Expand Employee data button is not present",true);
			Assert.fail();
		}
		
	}

	public void clickFilter() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(Filter));
			Filter.click();
		}
		catch(Exception e)
		{
			Reporter.log("Filter button is not present",true);
			Assert.fail();
		}
		
	}

	public void verifydept() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(FilterDept));
			Reporter.log(FilterDept.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("FilterDept button is not present",true);
			Assert.fail();
		}	
	}

	public void verifyLoc()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(FilterLoc));
			Reporter.log(FilterLoc.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("FilterLoc button is not present",true);
			Assert.fail();
		}	
		
	}

	public void verifyStartDate() {
		try{
			wait.until(ExpectedConditions.visibilityOf(FilterStartDate));
			Reporter.log(FilterStartDate.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("FilterStartDate button is not present",true);
			Assert.fail();
		}	
		
	}

	public void verifyEndDate() {
		try{
			wait.until(ExpectedConditions.visibilityOf(FilterEndDate));
			Reporter.log(FilterEndDate.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("FilterEndDate button is not present",true);
			Assert.fail();
		}	
		
	}

	public void verifyReset() {
		try{
			wait.until(ExpectedConditions.visibilityOf(FilterReset));
			Reporter.log(FilterReset.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("FilterReset button is not present",true);
			Assert.fail();
		}	
		
	}

	public void verifyApply() {
		try{
			wait.until(ExpectedConditions.visibilityOf(FilterApply));
			Reporter.log(FilterApply.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("FilterApply button is not present",true);
			Assert.fail();
		}	
		
	}
}