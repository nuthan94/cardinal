package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import generic.BasePageCardinal;

public class ProfileCardinal extends BasePageCardinal
{

	public ProfileCardinal(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="/html/body/app-dashboard/header/ul[2]/li[3]/a")
	private WebElement profile;
	
	@FindBy(xpath="/html/body/app-dashboard/header/ul[2]/li[3]/div/a")
	private WebElement logout;
	
	WebDriverWait wait=new WebDriverWait(driver, 10);

	public void clickProfile()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(profile));
			profile.click();
		}
		catch(Exception e)
		{
			Reporter.log("profile button is not present",true);
			
		}
		
	}

	public void clickLogout() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(logout));
			logout.click();
		}
		catch(Exception e)
		{
			Reporter.log("Logout button is not present",true);
		}
	}

}
