	package pom;

import static org.testng.Assert.fail;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.rewrite.PropertyRewritePolicy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.BasePageCardinal;

public class MasterCardinal extends BasePageCardinal
{
	public MasterCardinal(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//a[@href='#/master']")
	private WebElement masterTAB;				//Master tab
	
	@FindBy(xpath="//button[.='Employee']")
	private WebElement employeeTAB;				//Employee tab
	
	@FindBy(xpath="//button[.='Store']")
	private WebElement StoreTAB;
	
	@FindBy(xpath="//button[@touranchor='master.dltEmployee']")
	private WebElement deleteEmployee;			//Delete employee
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[1]/div/div/div[4]/table/tbody/tr/td[2]/label/input")
	private WebElement Search_Delete1;
	
	@FindBy(xpath="/html/body/div/div/div[10]/button[1]")
	private WebElement DeleteNo;
	
	@FindBy(xpath="/html/body/div/div/div[10]/button[2]")
	private WebElement DeleteYes;
	
	@FindBy(xpath="/html/body/div[2]/div/div[10]/button[1]")
	private WebElement deleteOK;
					
	@FindBy(xpath="//button[@touranchor='master.addEmployee']")
	private WebElement addEmployee;				//Add employee
	
	@FindBy(xpath="/html/body/div/div/div[10]/button[1]")
	private WebElement EmpCreationOK;
	
	@FindBy(xpath="//button[@touranchor='master.export']")
	private WebElement exportEmployees;			//Export Employee
	
	@FindBy(xpath="//*[@id='appendedInputButton']")
	private WebElement employeeSearchTEXT;		//Employee search
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[1]/div/div/div[1]/div/div[2]/div/span/button")
	private WebElement employeeSearchBTN;		//Employee search button
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[1]/div/div/div[1]/div/div[2]/div/span[1]")
	private WebElement employeeSearchCancel;
	
	@FindBy(xpath="//button[.='Take a Tour']")
	private WebElement masterTOUR;				//Take a tour
					//app-dashboard/div/main/div/app-master/div[2]/div[2]/div/div/div[4]/table/tbody/tr/td[9]
	@FindBy(xpath="//app-master/div[2]/div[2]/div/div/div[3]/table/tbody/tr[1]/td[9]")
	private WebElement EDITemployee;			//Edit employee (1)
	
	@FindBy(xpath="//*[@id='appendedInputButton']")
	private WebElement editEmpSearch;
	
	@FindBy(xpath="username9")
	private WebElement EDIT_ENumber;			//Edit employee number
	
	@FindBy(xpath="username3")
	private WebElement EDIT_REGNumber;			//Edit register number
	
	@FindBy(xpath="//*[@id='select1']")
	private WebElement EDIT_EmployeeType;		//Employee Employee typr
	
	@FindBy(xpath="//*[@id='email3']")
	private WebElement EDIT_Name;				// Edit employee name
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-empcreate/div[2]/div/div[2]/tabset/div/tab/div[2]/form/div[5]/div/select")
	private WebElement EDIT_Gender;				//Edit gender
	
	@FindBy(xpath="//*[@id='select5']")
	private WebElement EDIT_Location;			//Edit location
	
	@FindBy(xpath="//*[@id='select']")  
	private WebElement EDIT_Role;				//Edit Role
	
	@FindBy(xpath="//*[@id='select2']")
	private WebElement EDIT_Designation;		//Edit Designation
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/tabset/div/tab/div[2]/form/div[8]/div/div[2]/angular2-multiselect/div/div[1]/div")
	private WebElement EDIT_Department;			//Edit department
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/tabset/div/tab/div[2]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[1]/label")
	private WebElement EDIT_Dept_Market;		//Edit Dept Market
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/tabset/div/tab/div[2]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[2]/label")
	private WebElement EDIT_Dept_Testing;		//Edit dept Testing
	
	@FindBy(xpath="//*[@id='birth3']")
	private WebElement EDIT_DOB_Input;			//Edit DOB
	
	//Calendar component
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/tabset/div/tab/div[2]/form/div[10]/div/div/input")
	private WebElement EDIT_Store_Search;		//edit store
	
	@FindBy(xpath="//button[.='Cancel']")
	private WebElement EDIT_Cancel;				//cancel button
	
	@FindBy(xpath="//button[.='Submit']")
	private WebElement EDIT_Submit;				//submit button
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/tabset/div/tab/div[1]/span[2]/i")
	private WebElement EDIT_Status;          //Active-Inactive
	
	@FindBy(xpath="//*[@id='emp3']")
	private WebElement Add_ENum;			//Employee number
	//*[@id="reg3"]
	@FindBy(xpath="//*[@id='reg3']")
	private WebElement Add_RegNum;			//Register number
	
	@FindBy(xpath="//*[@id='select1']")
	private WebElement Add_EmpType;			//Employee type
	
	@FindBy(xpath="//*[@id='email3']")
	private WebElement Add_Name;			//Employee name
			    	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[5]/div/select")
	private WebElement Add_Gender;			//Gender
	
	@FindBy(xpath="//*[@id='loc5']")
	private WebElement Add_Location;		//Location
	
	@FindBy(xpath="//*[@id='select']")
	private WebElement Add_Role;			//Role (User/Admin)
	
	@FindBy(xpath="//*[@id='select3']")
	private WebElement Add_Designation;		//Designation
					
	@FindBy(xpath="	")
	private WebElement Add_Dept;			//Department
	///html/body/app-dashboard/div/main/div/app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/div[1]
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/div[1]")
	private WebElement UnselectAllDept;	
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[1]/label")
	private WebElement Add_Dept_Market;		//Market Department
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[2]/label")
	private WebElement Add_Dept_Testing;	//Testing Department
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[3]/label")
	private WebElement Add_Dept_Sales;
					
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[4]/label")
	private WebElement Add_Dept_Construction;
	
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[8]/div/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[5]/label")
	private WebElement Add_Dept_Finance;
	
	@FindBy(xpath="//*[@id='birth3']")
	private WebElement Add_DOB_input;		//DOB
					
	@FindBy(xpath="//app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[10]/div/div/input")
	private WebElement Add_Store;			//Store
				
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[10]/div/div/span[2]/button")
	private WebElement Add_Store_BTN;

	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-empcreate/div[2]/div/div[2]/div/div[3]/form/div[10]/div[2]/div[1]/div[2]")
	private WebElement storeSearch1;
	
	@FindBy(xpath="//button[.='Cancel']")
	private WebElement Add_Cancel;			//Cancel
	
	@FindBy(xpath="//button[.='Submit']")
	private WebElement Add_Submit;			//Submit
	
//	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[2]/div/div/div[1]/div/div[1]/i")
//	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[2]/div/div/div[1]/div/div[1]")
	@FindBy(xpath="//div[@class='col-sm-6 empStyle']")
//	@FindBy(xpath="//div//i[@class='fa fa-user fa-lg m-t-2']")
	private WebElement EmployeeCount;		//Employee count
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[2]/div/div/div[2]/table/thead/tr/th[2]/label/input")
	private WebElement DeleteAllCHECKBOX;	//Delete all checkbox
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-master/div[2]/div[2]/div/div/div[3]/table/tbody/tr[1]/td[2]/label/input")
	private WebElement checkbox1;			//checkbox 1
	
	@FindBy(xpath="//*[@id='swal2-content']")
	private WebElement empcreationmsg;
	
	@FindBy(xpath="//div[.='Employee number already exists']")
	private WebElement empAlreadyExistERROR_MSG;
	
	@FindBy(xpath="/html/body/div[2]/div/div[10]/button[1]")
	private WebElement empAlreadyExistERROR_MSG_OK;
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/ng-component/div[1]/div[2]/div[1]/div/div/div[2]/div/i")
	private WebElement empSearchCancel;
	
	@FindBy(xpath="//span[.='Employee']")
	private WebElement EmployeeText;
	
	@FindBy(xpath="//input[@placeholder='Employee Number or Name or Status']")
	private WebElement EmployeeSearch;
	
	@FindBy(xpath="//button[@touranchor='master.showall']")
	private WebElement MasterShowAll;
	
	@FindBy(xpath="//span[.='Status']")
	private WebElement EStatus;
	
	@FindBy(xpath="//span[.='Role']")
	private WebElement ERole;
	
	@FindBy(xpath="//span[.='Stores']")
	private WebElement EStores;
	
	@FindBy(xpath="//span[.='Last Log']")
	private WebElement ELastLog;
	
	@FindBy(xpath="//span[.='Edit']")
	private WebElement EEdit;
	
	WebDriverWait wait=new WebDriverWait(driver, 20); //Explicit wait
	
	public void clickMasterData() 
	{
	//	WebDriverWait wait=new WebDriverWait(driver, 20);
		try{
		wait.until(ExpectedConditions.visibilityOf(masterTAB));
		Thread.sleep(3000);
		masterTAB.click();
		}
		catch(Exception e)
		{
			Reporter.log("MasterTAB not displayed",true);
			Assert.fail();
		}
	}

	public void clickAddEmployee() throws InterruptedException 
	{
	//	WebDriverWait wait=new WebDriverWait(driver, 20);
		try{
			Thread.sleep(6000);
			wait.until(ExpectedConditions.visibilityOf(addEmployee));
			addEmployee.click();
		}
		catch (Exception e) {
			Reporter.log("Add employee button not displayed",true);
			Assert.fail();
		}
	}


	
	public void selectLocation(String location) 
	{
		try{
		Select select=new Select(Add_Location);
		select.selectByVisibleText(location);
		}
		catch(Exception e)
		{
			Reporter.log("Add_Location not displayed",true);
			Assert.fail();
		}
	}

	public void selectRole(String Role) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Add_Role));
		Select select=new Select(Add_Role);
		select.selectByVisibleText(Role);
		}
		catch(Exception e)
		{
			Reporter.log("Add_Role not displayed",true);
			Assert.fail();
		}
	}

	public void selectDesignation(String designation) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Add_Designation));
		Select select=new Select(Add_Designation);
		select.selectByVisibleText(designation);
		}
		catch(Exception e)
		{
			Reporter.log("Add_Designation not displayed",true);
			Assert.fail();
		}
	}

	public void selectDepartment(String department) throws InterruptedException 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Add_Dept));
		Add_Dept.click();
		Thread.sleep(3000);
		UnselectAllDept.click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOf(Add_Dept_Market));
		Add_Dept_Market.click();
		Add_Dept_Testing.click();
		Add_Dept_Finance.click();
		Add_Dept.click();
		}
		catch(Exception e)
		{
			Reporter.log("Department not displayed",true);
			Assert.fail();
		}
	}

	public void setDOB(String DOB) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Add_DOB_input));
		Add_DOB_input.sendKeys(DOB);
		}
		catch (Exception e) {
			Reporter.log("Add_DOB_input not displayed",true);
			Assert.fail();
		}
	}

	public void clickSubmit() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Add_Submit));
		Add_Submit.click();		
		}
		catch (Exception e) {
			Reporter.log("Add_Submit not displayed",true);
			Assert.fail();
		}
	}

	public void clickOK() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(empcreationmsg));
		driver.navigate().refresh();
	//	empcreationmsg.click();		
	}
		catch (Exception e) {
			Reporter.log("empcreationmsg not displayed",true);
			Assert.fail();
		}
		}

	public void empCount() 
	{
		try{
		String count=EmployeeCount.getText();
		Reporter.log(count,true);	
		}
		catch (Exception e) {
			Reporter.log("Add_ENum not displayed",true);
			Assert.fail();
		}
	}

	public void clickEmpsearch(String enumber) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(employeeSearchTEXT));
		employeeSearchTEXT.sendKeys(enumber);
		employeeSearchBTN.click();
		Thread.sleep(5000);
		Reporter.log(enumber+" created successfully",true);
		employeeSearchCancel.click();
		
		}
		catch (Exception e) {
			Reporter.log("employeeSearchTEXT not displayed",true);
			Assert.fail();
		}
	}

	public void clickEditSearchBTN() throws InterruptedException 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(employeeSearchBTN));
		employeeSearchBTN.click();	
		Thread.sleep(3000);
		}
		catch (Exception e) {
			Reporter.log("employeeSearchBTN not displayed",true);
			Assert.fail();
		}
	}	
	
	public void clickEditBTN() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(editEmpSearch));
		editEmpSearch.click();	
		}
		catch (Exception e) {
			Reporter.log("editEmpSearch not displayed",true);
			Assert.fail();
		}
	}

	public void changeName(String namechange) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(EDIT_Name));
		EDIT_Name.clear();
		EDIT_Name.sendKeys(namechange);		
		}
		catch (Exception e) {
			Reporter.log("EDIT_Name not displayed",true);
			Assert.fail();
		}
	}

	public void changeDOB(String changeDOB) 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(EDIT_DOB_Input));
		EDIT_DOB_Input.clear();
		EDIT_DOB_Input.sendKeys(changeDOB);
		}
		catch (Exception e) {
			Reporter.log("EDIT_DOB_Input not displayed",true);
			Assert.fail();
		}
	}

	public void addStore(String store) throws InterruptedException 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(EDIT_Submit));
		EDIT_Store_Search.click();
		EDIT_Store_Search.sendKeys(store);
		Thread.sleep(2000);
		storeSearch1.click();
		EDIT_Submit.click();
		}
		catch (Exception e) {
			Reporter.log("EDIT_Submit not displayed",true);
			Assert.fail();
		}
	}

	public void clickDelete() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(deleteEmployee));
		deleteEmployee.click();	
		}
		catch (Exception e) {
			Reporter.log("deleteEmployee not displayed",true);
			Assert.fail();
		}
	}

	public void clickNo() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(DeleteNo));
		DeleteNo.click();
		}
		catch (Exception e) {
			Reporter.log("DeleteNo not displayed",true);
			Assert.fail();
		}
	}

	public void clickYes() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(DeleteYes));
		Reporter.log("Clicking on YES. User deleted",true);
		DeleteYes.click();
		}
		catch (Exception e) {
			Reporter.log("DeleteYes not displayed",true);
			Assert.fail();
		}
	}

	public void clickDelCheckbox() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Search_Delete1));
		Search_Delete1.click();
		}
		catch (Exception e) {
			Reporter.log("Search_Delete1 not displayed",true);
			Assert.fail();
		}
	}

	public void clickDelOK() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(deleteOK));
			deleteOK.click();	
		}
		catch (Exception e) {
			Reporter.log("deleteOK not displayed",true);
			Assert.fail();
		}
	}

	public void verifyMaster() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(masterTAB));
		Reporter.log(masterTAB.getText(),true);
		}
		catch (Exception e) {
			Reporter.log("masterTAB not displayed",true);
			Assert.fail();
		}
	}

	public void clickEmpCreationOK()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EmpCreationOK));
			EmpCreationOK.click();
			Thread.sleep(5000);
		}
		catch (Exception e) {
			Reporter.log("EmpCreationOK not displayed",true);
			Assert.fail();
		}
	}

	public void clickEmployeeSearch(String A_Enumber) {
		try{
			wait.until(ExpectedConditions.visibilityOf(employeeSearchTEXT));
			employeeSearchTEXT.click();
			employeeSearchTEXT.sendKeys(A_Enumber);
			employeeSearchBTN.click();
			}
		catch (Exception e) {
			Reporter.log("employeeSearchTEXT not displayed",true);
			Assert.fail();
		}
		
	}

	public void verifyErrorMessage() {
		try{
			wait.until(ExpectedConditions.visibilityOf(empAlreadyExistERROR_MSG));
			Reporter.log("ERROR MESSAGE: "+empAlreadyExistERROR_MSG.getText(),true);
			wait.until(ExpectedConditions.visibilityOf(empAlreadyExistERROR_MSG_OK));
			empAlreadyExistERROR_MSG_OK.click();
		}
		catch (Exception e) {
			Reporter.log("empAlreadyExistERROR_MSG not displayed",true);
			Assert.fail();
		}
		
	}

	public void clickEmpSearchCancel() {
		try{
			wait.until(ExpectedConditions.visibilityOf(employeeSearchTEXT));
			Thread.sleep(3000);
			employeeSearchTEXT.clear();
			wait.until(ExpectedConditions.visibilityOf(employeeSearchBTN));
			employeeSearchBTN.click();
			Thread.sleep(3000);
			
		}
		catch(Exception e){
			Reporter.log("employeeSearchTEXT not displayed",true);
			Assert.fail();
		}
	}

	public void setStore(String Store) {
		try{
			wait.until(ExpectedConditions.visibilityOf(Add_Store));
			Add_Store.sendKeys(Store);
			wait.until(ExpectedConditions.visibilityOf(Add_Store_BTN));
			Add_Store_BTN.click();
			wait.until(ExpectedConditions.visibilityOf(storeSearch1));
			storeSearch1.click();			
		}
		catch (Exception e)
		{
			Reporter.log("Add_Store not displayed",true);
			Assert.fail();
		}
		
	}

	public void clickEditEmpsearch(String A_Enumber) {
		try{
			wait.until(ExpectedConditions.visibilityOf(editEmpSearch));
			editEmpSearch.sendKeys(A_Enumber);
			employeeSearchBTN.click();			
		}
		catch (Exception e) {
			Reporter.log("editEmpSearch not displayed",true);
			Assert.fail();
		}
		
	}

	public void verifyEmp() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(employeeTAB));
			Reporter.log(employeeTAB.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("employeeTAB is not present",true);
			Assert.fail();
		}
	}

	public void verifyStore() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(StoreTAB));
			Reporter.log(StoreTAB.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("StoreTAB is not present",true);
			Assert.fail();
		}
		
		
	}

	public void verifyOther() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(StoreTAB));
		Reporter.log(StoreTAB.getText(),true);
	}
		catch(Exception e)
		{
			Reporter.log("StoreTAB is not present",true);
			Assert.fail();
		}
	}


	public void verifyEmployeeTab() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(employeeTAB));
			Reporter.log(employeeTAB.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("employeeTAB is not present",true);
			Assert.fail();	
		}
	}

	public void verifyStoreTab()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(StoreTAB));
			Reporter.log(StoreTAB.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("StoreTAB is not present",true);
			Assert.fail();	
		}
	}

	public void verifyEmployee() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EmployeeText));
			Reporter.log(EmployeeText.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("EmployeeText is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyEmployeeSearch()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EmployeeSearch));
			Reporter.log(EmployeeSearch.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("EmployeeSearch is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyShowAll() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(MasterShowAll));
			Reporter.log(MasterShowAll.getAttribute("touranchor"),true);
		}
		catch(Exception e)
		{
			Reporter.log("MasterShowAll is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyDeleteEmployee() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(deleteEmployee));
			Reporter.log(deleteEmployee.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("deleteEmployee is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyAddEmployee()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(addEmployee));
			Reporter.log(addEmployee.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("addEmployee is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyExport()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(exportEmployees));
			Reporter.log(exportEmployees.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("exportEmployees is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyTakeATourMaster()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(masterTOUR));
			Reporter.log(masterTOUR.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("masterTOUR is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyMStatus()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EStatus));
			Reporter.log(EStatus.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("EStatus is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyMRole() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ERole));
			Reporter.log(ERole.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("Role is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyMStores()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EStores));
			Reporter.log(EStores.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("EStores is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyMLastLog() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(ELastLog));
			Reporter.log(ELastLog.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("ELastLog is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyMEmployeeEdit()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EEdit));
			Reporter.log(EEdit.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("EEdit is not displayed",true);
			Assert.fail();
		}
	}

	public void clickStoreTAB() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(StoreTAB));
			StoreTAB.click();
		}
		catch(Exception e)
		{
			Reporter.log("StoreTAB is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyAddStoreTAB() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(StoreTAB));
			StoreTAB.click();
		}
		catch(Exception e)
		{
			Reporter.log("StoreTAB is not displayed",true);
			Assert.fail();
		}
	}

	public void clickCancel() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(Add_Cancel));
			Add_Cancel.click();
		}
		catch(Exception e)
		{
			Reporter.log("Cancel button is not present",true);
			Assert.fail();
		}
	}
}