package test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import generic.BaseTestCardinal;
import generic.ExcelCardinal;
import pom.EmployeeCardinal;
import pom.ForgotPassword;
import pom.LoginPageCardinal;
import pom.LogsCardinal;
import pom.MasterCardinal;
import pom.ProfileCardinal;
import pom.ReportCardinal;
import pom.SettingsCardinal;
import pom.StoreCardinal;

public class CardinalTestRun extends BaseTestCardinal
{
	static int row=0;
	static int coulumn=0;
	static String enumber=ExcelCardinal.getCellData(INPUT_PATH, "Credentials", 1, 0);
	static String DD=ExcelCardinal.getCellData(INPUT_PATH, "Credentials", 1, 1);
	static String MM=ExcelCardinal.getCellData(INPUT_PATH, "Credentials", 1, 2);
	static String YYYY=ExcelCardinal.getCellData(INPUT_PATH, "Credentials", 1, 3);
	static String A_Enumber=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 0);
	static String Reg_number=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 1);
	static String EmpType=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 2);
	static String Name=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 3);
	static String Gender=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 4);
	static String Location=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 5);
	static String Role=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 6);
	static String Designation=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 7);
	static String Department=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 8);
	static String DOB=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 9);
	static String Store=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 1, 10);
	
//	static int enumber_r=1;
//	static int enumber_c=1;
//	static int ename_r=1;
//	static int ename_c=2;
//	static String A_Enumber1=ExcelCardinal.getCellData(INPUT_PATH, "Pagination", enumber_r, enumber_c);
//	static String Name1=ExcelCardinal.getCellData(INPUT_PATH, "Pagination", ename_r, ename_c);
	
//	static int anum=1;
//	static String aaenumber="NEPPY";
//	static int aname=1;
//	static String aaename="Universal";
	//StoreTAB
	static String StoreNumber=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 22, 0);
	static String StoreName=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 22, 1);
	static String StoreAddress=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 22, 2);
	static String StoreRadius=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 22, 3);
	static String StoreLatitude=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 22, 4);
	static String StoreLongitude=ExcelCardinal.getCellData(INPUT_PATH, "AddEmployee", 22, 5);
	
	
//	@Test(priority=1)
//	public void testLogin() throws InterruptedException, IOException
//	{	
//		LoginPageCardinal l=new LoginPageCardinal(driver);
//		ReportCardinal r=new ReportCardinal(driver);
//		ProfileCardinal p1=new ProfileCardinal(driver);
//		
//		
//		driver.manage().window().maximize();
//		Reporter.log("Scenario : Elements present in Login page are: ",true);	
//		l.verifyenumber();
//		l.verifyDD();
//		l.verifyMM();
//		l.verifyYYYY();
//		l.verifyLoginButton();	
//		l.verifyForgotPassword();
//	
//		Reporter.log("\nScenario : Enter invalid Employee number",true);
//		l.setEnumber("invalid");
//		l.setDate("12");
//		l.setMonth("02");
//		l.setYear("1990");
//		l.clickSubmit();
//		Thread.sleep(2000);
//		
//		TakesScreenshot t=(TakesScreenshot) driver;
//		File srcFile=t.getScreenshotAs(OutputType.FILE);
//		File destFile=new File("./C_screenshots/invalid_login_credentials.png");
//		FileUtils.copyFile(srcFile, destFile);
//		Reporter.log("Screenshot taken for invalid login credentials",true);
//		
//		driver.navigate().refresh();
//
//		Reporter.log("\nScenario : Enter INVALID DATE FORMAT with VALID EMPLOYEE NUMBER",true);
//		l.setEnumber("112233");
//		l.setDate("99");
//		l.setMonth("09");
//		l.setYear("9999");
//		l.clickSubmit();
//		
//		TakesScreenshot t1=(TakesScreenshot) driver;
//		File srcFile1=t1.getScreenshotAs(OutputType.FILE);
//		File destFile1=new File("./C_screenshots/invalid_date_format.png");
//		FileUtils.copyFile(srcFile1, destFile1);
//		Reporter.log("Screenshot taken for invalid date format",true);
//			
//		Reporter.log("\nScenario : Enter invalid DOB/Password with valid Employee number",true);
//		l.setEnumber("112233");
//		l.setDate("01");
//		l.setMonth("02");
//		l.setYear("1997");
//		l.clickSubmit();
//		
//		TakesScreenshot t2=(TakesScreenshot) driver;
//		File srcFile2=t2.getScreenshotAs(OutputType.FILE);
//		File destFile2=new File("./C_screenshots/Invalid_DOB.png");
//		FileUtils.copyFile(srcFile2, destFile2);
//		Reporter.log("Screenshot taken for Invalid DOB",true);
//		
//		Reporter.log("\nScenario : Login as Permanent Admin",true);
//		l.setEnumber(enumber);
//		l.setDate(DD);
//		l.setMonth(MM);
//		l.setYear(YYYY);
//		l.clickSubmit();
//		r.verifyReport();
//		Reporter.log("Permanent admin has access to web",true);
//		p1.clickProfile();
//		p1.clickLogout();
//	
//		Reporter.log("\nScenario : Login as Temporary Admin",true);
//		l.setEnumber("T4400710000");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		r.verifyReport();
//		Reporter.log("Temporary Admin has access to web",true);
//		ProfileCardinal p=new ProfileCardinal(driver);
//		p.clickProfile();
//		p.clickLogout();
//		
//		Reporter.log("\nScenario : Login as Deleted permanent admin",true);
//		l.setEnumber("DELPERADMIN");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t3=(TakesScreenshot) driver;
//		File srcFile3=t3.getScreenshotAs(OutputType.FILE);
//		File destFile3=new File("./C_screenshots/Deleted_permanent_admin_login.png");
//		FileUtils.copyFile(srcFile3, destFile3);
//		Reporter.log("Screenshot taken for: Deleted permanent admin login",true);
//		
//		Reporter.log("\nScenario : Login as Deleted temporary admin",true);
//		l.setEnumber("T1992410000");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t4=(TakesScreenshot) driver;
//		File srcFile4=t4.getScreenshotAs(OutputType.FILE);
//		File destFile4=new File("./C_screenshots/Deleted_temporary_admin_login.png");
//		FileUtils.copyFile(srcFile4, destFile4);
//		Reporter.log("Screenshot taken for: Deleted temporary admin login",true);
//		
//		Reporter.log("\nScenario : Login as Deactivated permanent admin",true);
//		l.setEnumber("T1992410000"); //Test data
//		l.setDate("01");//Test data
//		l.setMonth("01");//Test data
//		l.setYear("2018");//Test data
//		l.clickSubmit();
//		TakesScreenshot t5=(TakesScreenshot) driver;
//		File srcFile5=t5.getScreenshotAs(OutputType.FILE);
//		File destFile5=new File("./C_screenshots/Deactivated_permanent_admin_login.png");
//		FileUtils.copyFile(srcFile5, destFile5);
//		Reporter.log("Screenshot taken for: Deactivated permanent admin login",true);
//		
//		Reporter.log("\nScenario : Login as Deactivated temporary admin",true);
//		l.setEnumber("T1992410000"); //Test data
//		l.setDate("01");//Test data
//		l.setMonth("01");//Test data
//		l.setYear("2018");//Test data
//		l.clickSubmit();
//		TakesScreenshot t6=(TakesScreenshot) driver;
//		File srcFile6=t6.getScreenshotAs(OutputType.FILE);
//		File destFile6=new File("./C_screenshots/Deactivated_temporary_admin_login.png");
//		FileUtils.copyFile(srcFile6, destFile6);
//		Reporter.log("Screenshot taken for: Deactivated temporary admin login",true);
//	
//		Reporter.log("\nScenario : Login as Permanent Department Admin",true);
//		l.setEnumber("Permanent Department Admin");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		ReportCardinal r1=new ReportCardinal(driver);
//		r1.verifyReport();
//		Reporter.log("Permanent Department Admin has access to web",true);
//		p1.clickProfile();
//		p1.clickLogout();
//		
//		Reporter.log("\nScenario : Login as Temporary Department Admin",true);
//		l.setEnumber("T5435610000");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		r1.verifyReport();
//		Reporter.log("Temporary Department Admin has access to web",true);
//		p1.clickProfile();
//		p1.clickLogout();
//		
//		Reporter.log("\nScenario : Login as Deleted permanent dept admin",true);
//		l.setEnumber("DELPERDEPADN");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t7=(TakesScreenshot) driver;
//		File srcFile7=t7.getScreenshotAs(OutputType.FILE);
//		File destFile7=new File("./C_screenshots/Deleted_permanent_DEPT_admin_login.png");
//		FileUtils.copyFile(srcFile7, destFile7);
//		Reporter.log("Screenshot taken for: Deleted permanent dept admin login",true);
//		
//		Reporter.log("\nScenario : Login as Deleted temporary dept admin",true);
//		l.setEnumber("T8276110000");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t8=(TakesScreenshot) driver;
//		File srcFile8=t8.getScreenshotAs(OutputType.FILE);
//		File destFile8=new File("./C_screenshots/Deleted_temporary_DEPT_admin_login.png");
//		FileUtils.copyFile(srcFile8, destFile8);
//		Reporter.log("Screenshot taken for: Deleted temporary dept admin login",true);
//		
//		Reporter.log("\nScenario : Login as Deactivated department permanent admin",true);
//		l.setEnumber("T1992410000"); //Test data
//		l.setDate("01");//Test data
//		l.setMonth("01");//Test data
//		l.setYear("2018");//Test data
//		l.clickSubmit();
//		TakesScreenshot t9=(TakesScreenshot) driver;
//		File srcFile9=t9.getScreenshotAs(OutputType.FILE);
//		File destFile9=new File("./C_screenshots/Deactivated_permanent_dept_admin_login.png");
//		FileUtils.copyFile(srcFile9, destFile9);
//		Reporter.log("Screenshot taken for: Deactivated permanent department admin login",true);
//		
//		Reporter.log("\nScenario : Login as Deactivated temporary department admin",true);
//		l.setEnumber("T1992410000"); //Test data
//		l.setDate("01");//Test data
//		l.setMonth("01");//Test data
//		l.setYear("2018");//Test data
//		l.clickSubmit();
//		TakesScreenshot t10=(TakesScreenshot) driver;
//		File srcFile10=t10.getScreenshotAs(OutputType.FILE);
//		File destFile10=new File("./C_screenshots/Deactivated_temporary_dept_admin_login.png");
//		FileUtils.copyFile(srcFile10, destFile10);
//		Reporter.log("Screenshot taken for: Deactivated temporary dept admin login",true);
//		
//		Reporter.log("\nScenario : Login as Permanent User",true);
//		l.setEnumber("Permanent User");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t11=(TakesScreenshot) driver;
//		File srcFile11=t11.getScreenshotAs(OutputType.FILE);
//		File destFile11=new File("./C_screenshots/User_No_access.png");
//		FileUtils.copyFile(srcFile11, destFile11);
//		Reporter.log("Screenshot taken for: No access for User",true);
//		
//		Reporter.log("\nScenario : Login as Temporary User",true);
//		l.setEnumber("T7196610000");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t12=(TakesScreenshot) driver;
//		File srcFile12=t12.getScreenshotAs(OutputType.FILE);
//		File destFile12=new File("./C_screenshots/User_No_access.png");
//		FileUtils.copyFile(srcFile12, destFile12);
//		Reporter.log("Screenshot taken for: No access for User",true);
//		
//		Reporter.log("\nScenario : Login as Deleted permanent user",true);
//		l.setEnumber("DELPERUSR");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t13=(TakesScreenshot) driver;
//		File srcFile13=t13.getScreenshotAs(OutputType.FILE);
//		File destFile13=new File("./C_screenshots/Deleted_PERM_user_login.png");
//		FileUtils.copyFile(srcFile13, destFile13);
//		Reporter.log("Screenshot taken for: Deleted permanent user login",true);
//		
//		Reporter.log("\nScenario : Login as Deleted temporary user",true);
//		l.setEnumber("T4323310000");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t14=(TakesScreenshot) driver;
//		File srcFile14=t14.getScreenshotAs(OutputType.FILE);
//		File destFile14=new File("./C_screenshots/Deleted_temporary_user_login.png");
//		FileUtils.copyFile(srcFile14, destFile14);
//		Reporter.log("Screenshot taken for: Deleted temporary user login",true);
//		
//		Reporter.log("\nScenario : Login as Deactivated permanent user",true);
//		l.setEnumber("T1992410000"); //Test data
//		l.setDate("01");//Test data
//		l.setMonth("01");//Test data
//		l.setYear("2018");//Test data
//		l.clickSubmit();
//		TakesScreenshot t15=(TakesScreenshot) driver;
//		File srcFile15=t15.getScreenshotAs(OutputType.FILE);
//		File destFile15=new File("./C_screenshots/Deactivated_permanent_user_login.png");
//		FileUtils.copyFile(srcFile15, destFile15);
//		Reporter.log("Screenshot taken for: Deactivated permanent user login",true);
//		
//		Reporter.log("\nScenario : Login as Deactivated temporary user",true);
//		l.setEnumber("T1992410000"); //Test data
//		l.setDate("01");//Test data
//		l.setMonth("01");//Test data
//		l.setYear("2018");//Test data
//		l.clickSubmit();
//		TakesScreenshot t16=(TakesScreenshot) driver;
//		File srcFile16=t16.getScreenshotAs(OutputType.FILE);
//		File destFile16=new File("./C_screenshots/Deactivated_temporary_user_login.png");
//		FileUtils.copyFile(srcFile16, destFile16);
//		Reporter.log("Screenshot taken for: Deactivated temporary user login",true);
//		
//		Reporter.log("\nScenario : Login as Permanent Leader",true);
//		l.setEnumber("Permanent Leader");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		r1.verifyReport();
//		Reporter.log("Permanent Leader has access to web",true);
//		p1.clickProfile();
//		p1.clickLogout();
//		
//		Reporter.log("\nScenario : Login as Temporary Leader",true);
//		l.setEnumber("T6031010000");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		r1.verifyReport();
//		Reporter.log("Temporary Leader has access to web",true);
//		p1.clickProfile();
//		p1.clickLogout();
//		
//		
//		Reporter.log("\nScenario : Login as Deleted permanent LEADER",true);
//		l.setEnumber("DELPERLDR");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		TakesScreenshot t17=(TakesScreenshot) driver;
//		File srcFile17=t17.getScreenshotAs(OutputType.FILE);
//		File destFile17=new File("./C_screenshots/Deleted_permanent_LEADER_login.png");
//		FileUtils.copyFile(srcFile17, destFile17);
//		Reporter.log("Screenshot taken for: Deleted permanent leader login",true);
//		
//		Reporter.log("\nScenario : Login as Deactivated permanent user",true);
//		l.setEnumber("T1992410000"); //Test data
//		l.setDate("01");//Test data
//		l.setMonth("01");//Test data
//		l.setYear("2018");//Test data
//		l.clickSubmit();
//		TakesScreenshot t18=(TakesScreenshot) driver;
//		File srcFile18=t18.getScreenshotAs(OutputType.FILE);
//		File destFile18=new File("./C_screenshots/Deactivated_permanent_user_login.png");
//		FileUtils.copyFile(srcFile18, destFile18);
//		Reporter.log("Screenshot taken for: Deactivated permanent user login",true);
//		
//		Reporter.log("\nScenario : Login as Deactivated temporary leader",true);
//		l.setEnumber("T1992410000"); //Test data
//		l.setDate("01");//Test data
//		l.setMonth("01");//Test data
//		l.setYear("2018");//Test data
//		l.clickSubmit();
//		TakesScreenshot t19=(TakesScreenshot) driver;
//		File srcFile19=t19.getScreenshotAs(OutputType.FILE);
//		File destFile19=new File("./C_screenshots/Deactivated_temporary_leader_login.png");
//		FileUtils.copyFile(srcFile19, destFile19);
//		Reporter.log("Screenshot taken for: Deactivated temporary leader login",true);
//				
//		Reporter.log("\nScenario : Leave blank for Employee number, DOB and click on Login button",true);
//		l.blank();
//	
//	}		
//	
//	@Test(priority=2)
//	public void testForgotPassword() throws InterruptedException
//	{
//		
//		LoginPageCardinal l=new LoginPageCardinal(driver);
//		ForgotPassword p=new ForgotPassword(driver);
//		
//		Reporter.log("\nScenario : Check functionality of Forgot password",true);
//		l.clickForgotPassword();
//		
//		Reporter.log("\nScenario : When user click on Forgot password. Verify your account page should display",true);
//		p.verifyaccountPage();
//		
//		Reporter.log("\nScenario : Check elements present in Verify your account page",true);
//		p.verifyEmployeeNumber();
//		p.verifyDOB();
//		p.verifyButton();
//		p.verifyCancelBTN();
//		
//		Reporter.log("\nScenario : Enter valid employee number and valid DOB. User should be taken to Reset password page",true);
//		p.setEmployeeNumber("112233");
//		p.setDD("01");
//		p.setMM("01");
//		p.setYYYY("2018");
//		p.clickVerify();
//		p.verifyResetPasswordPage();
//		
//		Reporter.log("\nScenario : Check elements present in Reset Passsword page",true);
//		p.verifyNewPassword();
//		p.verifyConfirmPassword();
//		p.verifySubmit();
//		p.verifyCancelBTN();
//		
//		Reporter.log("\nScenario : Check whether placeholder value is present for Date fields",true);
//		p.verifyNewPasswordDD();
//		p.verifyNewPasswordMM();
//		p.verifyNewPasswordYYYY();
//		p.verifyConfirmPasswordDD();
//		p.verifyConfirmPasswordMM();
//		p.verifyConfirmPasswordYYYY();
//		p.clickCancel();
//		
//		Reporter.log("\nScenario : Enter invalid employee number and valid DOB in forgot password page",true);
//		l.clickForgotPassword();
//		p.setEmployeeNumber("Invalid");
//		p.setDD("01");
//		p.setMM("01");
//		p.setYYYY("2018");
//		p.clickVerify();
//		p.errorMSG();
//		
//		Reporter.log("\nScenario : Enter Valid employee number and Invalid DOB in forgot password page",true);
//		p.setEmployeeNumber("112233");
//		p.setDD("02");
//		p.setMM("01");
//		p.setYYYY("2018");
//		p.clickVerify();
//		p.errorMSG();
//		
//		Reporter.log("\nScenario : Leave Employee number and DOB empty and click Submit",true);
//		p.setEmployeeNumber("");
//		p.setDD("");
//		p.setMM("");
//		p.setYYYY("");
//		p.checkVerifyDisable();
//		p.clickCancel();
//		
//		Reporter.log("\nScenario : Enter valid employee number and valid DOB and click Submit. Reset password page should display",true);
//		l.clickForgotPassword();
//		p.setEmployeeNumber("112233");
//		p.setDD("01");
//		p.setMM("01");
//		p.setYYYY("2018");
//		p.clickVerify();
//		p.verifyResetPasswordPage();
//		
//		Reporter.log("\nScenario : Enter new password  and confirm password LESSER than 31/12/1899",true);
//		p.setNewPasswordDD("31");
//		p.setNewPasswordMM("12");
//		p.setNewPasswordYYYY("1899");
//		p.setConfirmPasswordDD("31");
//		p.setConfirmPasswordMM("12");
//		p.setConfirmPasswordYYYY("1899");
//		p.clickSubmit();
//		p.errorMSG();
//		
//		Reporter.log("\nScenario : Mismatch new and confirm password",true);
//		p.setNewPasswordDD("01");
//		p.setNewPasswordMM("01");
//		p.setNewPasswordYYYY("2018");
//		p.setConfirmPasswordDD("01");
//		p.setConfirmPasswordMM("01");
//		p.setConfirmPasswordYYYY("2017");
//		p.clickSubmit();
//		p.verifyPasswordDoesntMatch();
//		
//		Reporter.log("\nScenario : Match both New and Confirm password and Click on Cancel",true);
//		p.setNewPasswordDD("01");
//		p.setNewPasswordMM("01");
//		p.setNewPasswordYYYY("2018");
//		p.setConfirmPasswordDD("01");
//		p.setConfirmPasswordMM("01");
//		p.setConfirmPasswordYYYY("2018");
//		p.clickCancel();
//		
//		Reporter.log("\nScenario : Match both New and Confirm password and Click on Submit",true);
//		l.clickForgotPassword();
//		p.setEmployeeNumber("112233");
//		p.setDD("01");
//		p.setMM("01");
//		p.setYYYY("2018");
//		p.clickVerify();
//		Thread.sleep(3000);
//		p.setNewPasswordDD("01");
//		p.setNewPasswordMM("01");
//		p.setNewPasswordYYYY("2018");
//		p.setConfirmPasswordDD("01");
//		p.setConfirmPasswordMM("01");
//		p.setConfirmPasswordYYYY("2018");
//		p.clickSubmit();
//		p.verifyMSG();
//		
//		Reporter.log("\nScenario : Enter old password",true);
//		l.setEnumber("112233");
//		l.setDate("01");
//		l.setMonth("02");
//		l.setYear("2018");
//		l.clickSubmit();
//		driver.navigate().refresh();
//		
//		Reporter.log("\nScenario : Enter New password",true);
//		l.setEnumber("112233");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();		
//	}
//	
//	@Test(priority=2)
//	public void testHomePage()
//	{
//		LoginPageCardinal l=new LoginPageCardinal(driver);
//		ReportCardinal r=new ReportCardinal(driver);
//		MasterCardinal m=new MasterCardinal(driver);
//		SettingsCardinal s=new SettingsCardinal(driver);
//		LogsCardinal lo=new LogsCardinal(driver);
//		l.setEnumber("112233");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		
//		Reporter.log("\nScenario : When logged in with valid credential, Report page should display",true);
//		Reporter.log("The selected tab is:",true);
//		r.verifyReport();
//				
//		Reporter.log("\nScenario : The tabs present in Homepage are: ",true);
//		r.verifyReport();
//		m.verifyMaster();
//		s.verifySettings();
//		lo.verifyLogs();	
//	}
//	
//	@Test(priority=3)
//	public void testReport() throws InterruptedException
//	{
//		LoginPageCardinal l=new LoginPageCardinal(driver);
//		l.setEnumber("112233");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		ReportCardinal r=new ReportCardinal(driver);
//		r.clickReport();
//		
//		Reporter.log("\nScenario : The elements in Report page are:",true);
//		r.verifyToday();
//		r.verifyYesterday();
//		r.verifyLast7Days();
//		r.verifyLast30Days();
//		r.verifyShowAll();
//		r.verifyFilter();
//		r.verifyTakeATour();
//		r.verifyEmployeeSearch();
//		
//		Reporter.log("\nScenario : Click on Today",true);	
////		Thread.sleep(5000);
////		r.clickReport();
//		r.clickMonth();
//		r.clickToday();
//		Thread.sleep(5000);
//		Reporter.log("Displays the attendance of Today i.e present day",true);
//		
//		Reporter.log("\nScenario : Click on Yesterday",true);	
//		r.clickYesterday();
//		Thread.sleep(5000);
//		Reporter.log("Displays the attendance of Yesterday i.e previous day",true);
//		
//		Reporter.log("\nScenario : Click on Last 7 Days",true);	
//		r.click7Days();
//		Thread.sleep(5000);
//		Reporter.log("Displays the attendance of Last 7 days",true);
//		
//		Reporter.log("\nScenario : Click on Last 30 days",true);	
//		r.clickMonth();
//		Thread.sleep(5000);
//		Reporter.log("Displays the attendance of 1 month i.e last 30 days",true);
//		
//		Reporter.log("\nScenario : Expand button to check the attendance of employee",true);
//		r.clickEmpExpand();
//		r.clickExpData();
//	}
//	
//	@Test(priority=4)
//	public void testMasterEmployee()
//	{
//		LoginPageCardinal l=new LoginPageCardinal(driver);
//		MasterCardinal m=new MasterCardinal(driver);
//		l.setEnumber("112233");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//			
//		Reporter.log("\nScenario : The tabs present in Master Data are:",true);
//		m.clickMasterData();
//		m.verifyEmployeeTab();
//		m.verifyStoreTab();
//		
//		Reporter.log("\nScenario : The elements present in Employee tab are:",true);
//		m.verifyEmployeeSearch();
//		m.verifyShowAll();
//		m.verifyDeleteEmployee();
//		m.verifyAddEmployee();
//		m.verifyExport();
//		m.verifyTakeATourMaster();
//		m.verifyEmployee();
//		m.verifyMStatus();
//		m.verifyMRole();
//		m.verifyMStores();
//		m.verifyMLastLog();
//		m.verifyMEmployeeEdit();	
//	}
//	
//	@Test(priority=5)
//	public void testMasterStore()
//	{
//		LoginPageCardinal l=new LoginPageCardinal(driver);
//		MasterCardinal m=new MasterCardinal(driver);
//		StoreCardinal s=new StoreCardinal(driver);
////		l.setEnumber("112233");
////		l.setDate("01");
////		l.setMonth("01");
////		l.setYear("2018");
////		l.clickSubmit();
//		
//		Reporter.log("\nScenario : The elements present in Store tab are:",true);
//		m.clickMasterData();
//		m.clickStoreTAB();
//		s.verifyAddStore();
//		s.verifyStoreExport();
//		s.verifyStoreSearch();
//		s.verifyStoreName();
//		s.verifyStoreAddress();
//		s.verifySetStore();
//		s.verifyStoreStatus();
//		s.verifyStoreEmployees();
//		s.verifyStoreEdit();
//	}
	
	@Test(priority=6)
	public void testAddEmployee() throws InterruptedException, IOException
	{
			
		LoginPageCardinal l=new LoginPageCardinal(driver);
		MasterCardinal m=new MasterCardinal(driver);
		EmployeeCardinal e=new EmployeeCardinal(driver);
		
		l.setEnumber("112233");
		l.setDate("01");
		l.setMonth("01");
		l.setYear("2018");
		l.clickSubmit();
		
//		Reporter.log("\nScenario : The elements present in Add employee page are: ",true);
//		m.clickMasterData();
//		e.clickAddEmployee();
//		e.verifyEnumber();
//		e.verifyRegNumber();
//		e.verifyEmpType();
//		e.verifyName();
//		e.verifyGender();
//		e.verifyLocation();
//		e.verifyRole();
//		e.verifyDesignation();
//		e.verifyDepartment();
//		e.verifyDOB();
//		e.verifyStore();
//		e.verifySubmit();
//		e.verifyCancel();
//		Thread.sleep(5000);
//		
//		driver.navigate().back();
		
		Reporter.log("\nScenario : Creating new Employee "+A_Enumber,true);
		
		m.clickMasterData();
	
		e.clickAddEmployee();
		Thread.sleep(4000);
	//	e.setEnumber(A_Enumber);
		e.setEnumber("2333333");
//		e.setRegNumber(Reg_number);
		e.setRegNumber("123");
//		e.selectEmployeeType(EmpType);	//Permanent-Temporary
		e.selectEmployeeType("Temporary");
//		e.setEname(Name);
		e.setEname("jkjkj");
//		e.selectGender(Gender);
		e.selectGender("Female");
//		e.selectLocation(Location);
//		e.selectRole(Role);
//		e.selectDesignation(Designation);
//		e.selectDepartment(Department);
//		e.setDOB(DOB);
//		e.setStore("Novaders");
//		e.clickSubmit();
//		e.clickEmpCreationOK();
//			
//		Reporter.log(A_Enumber+" created successfully\n",true);
//		e.clickEmpCreationOK();
//		
//		Reporter.log("Scenario : Search for employee number "+A_Enumber+" whether its created or not",true);		
//		
//		m.clickMasterData();
//		e.clickEmployeeSearch(A_Enumber);
//		TakesScreenshot a1=(TakesScreenshot) driver;
//		File srcadd1=a1.getScreenshotAs(OutputType.FILE);
//		File destadd1=new File("./C_screenshots/Search_Employee_created.png");
//		FileUtils.copyFile(srcadd1, destadd1);
//		Reporter.log("Screenshot taken to confirm New employee is created\n",true);
//		e.clickEmpSearchCancel();
//
//		Reporter.log("\nScenario : Create using existing Employee data "+A_Enumber,true);
//		MasterCardinal m1=new MasterCardinal(driver);
//		m.clickMasterData();
//		m.clickAddEmployee();
//		e.setEnumber(A_Enumber);
//		e.setRegNumber(Reg_number);
//		e.selectEmployeeType(EmpType);	//Permanent-Temporary
//		e.setEname(Name);
//		e.selectGender(Gender);
//		e.selectLocation(Location);
//		e.selectRole(Role);
//		e.selectDesignation(Designation);
//	//	m1.selectDepartment(Department);
//		e.setDOB(DOB);
//		e.clickSubmit();
//		e.verifyErrorMessage();		
	}
}
//	
//	@Test(priority=6)
//	public void testEditEmployee() throws InterruptedException, IOException
//	{
//		LoginPageCardinal l=new LoginPageCardinal(driver);
//		SettingsCardinal s=new SettingsCardinal(driver);
//	
//		l.setEnumber(enumber);
//		l.setDate(DD);
//		l.setMonth(MM);
//		l.setYear(YYYY);
//		l.clickSubmit();
//	
//		Reporter.log("Scenario  : Edit employee",true);
//		MasterCardinal m=new MasterCardinal(driver);
//		m.clickMasterData();
//		e.clickEditEmpsearch(A_Enumber);
//		e.clickEditBTN();
//		e.changeName("Chinmay");
//		e.changeDOB("20/01/1995");
//		e.addStore("Novaders");
//		driver.navigate().refresh();
//	
//		e.clickMasterData();
//		e.clickEmpsearch(enumber);
//		e.clickEditSearchBTN();
//		e.clickEditBTN();
//		Reporter.log("Screenshot taken\nChanges:1. "+Name+" changed to Chinmay.\n\tDOB "+DOB+" changed to 20/01/1995\n\tNovaders store added",true);
//		TakesScreenshot t=(TakesScreenshot) driver;
//		File srcFile=t.getScreenshotAs(OutputType.FILE);
//		File destFile=new File("./C_screenshots/EmployeeEdit.png");
//		FileUtils.copyFile(srcFile, destFile);
//	}
//
//	@Test(priority=7)
//	public void testEmpDelete() throws InterruptedException, IOException
//	{
//		Reporter.log("Delete employee: Emp number- "+A_Enumber,true);
//		MasterCardinal m=new MasterCardinal(driver);
//		m.clickMasterData();
//		e.clickEmpsearch(A_Enumber);
//		e.clickEditSearchBTN();
//		e.clickDelCheckbox();
//		e.clickDelete();
//		e.clickNo();
//		Reporter.log("Click on NO. User should not delete",true);
//		e.clickDelete();
//		e.clickYes();
//		e.clickDelOK();
//		Reporter.log("Searching for "+enumber,true);
//		e.clickEmpsearch(A_Enumber);
//		e.clickEditSearchBTN();
//		TakesScreenshot t=(TakesScreenshot) driver;
//		File srcFile=t.getScreenshotAs(OutputType.FILE);
//		File destFile=new File("./C_DeleteEmployee.png");
//		FileUtils.copyFile(srcFile, destFile);
//	
//	}
//}
//	
//	@Test(priority=5)
//	public void testFilter()
//	{
//		
//
//		LoginPageCardinal l=new LoginPageCardinal(driver);
//		l.setEnumber("112233");
//		l.setDate("01");
//		l.setMonth("01");
//		l.setYear("2018");
//		l.clickSubmit();
//		
//		ReportCardinal r=new ReportCardinal(driver);
//		
//		Reporter.log("Check functionality of Filter",true);
//		r.clickFilter();
//		Reporter.log("Scenario : Check elements present in Filter\nThe elements present in Filter are : ",true);
//		r.verifydept();
//		r.verifyLoc();
//		r.verifyStartDate();
//		r.verifyEndDate();
//		r.verifyReset();
//		r.verifyApply();
//		driver.findElement(By.xpath("/html/body/app-dashboard/div/main/div/ng-component/div[4]/div/div/div[1]/button")).click();
//	}
//	
//	
//	@Test(priority=4)
//	public void testSettings() throws InterruptedException
//	{
//		LoginPage l=new LoginPage(driver);
//		Settings s=new Settings(driver);
//		
//		l.setEnumber(enumber);
//		l.setDate(DD);
//		l.setMonth(MM);
//		l.setYear(YYYY);
//		l.clickSubmit();	
//		
//		Reporter.log("\nScenario : The elements present in Settings are:",true);
//		s.clickSettings();
//		s.verifyOrganisationSettings();
//		s.verifyPreferences();
//		s.verifyExport();
//		s.verifyImport();
//
//		s.clickOrganisationSettings();
//		
//		Reporter.log("\nScenario : Clear the radius value. Leave Radius value blank and click on Submit",true);
//		s.radiusBlank();
//		
//		Reporter.log("\nScenario : Enter alphabets to Radius",true);
//		s.setRadius("Alphabets");
//		
//		Reporter.log("\nScenario : Enter Special characters to Radius",true);
//		s.setRadius("!@#$%^&*");
//		
//		Reporter.log("\nScenario : Enter numbers",true);
//		s.setRadius("20000");	
//		
//		Reporter.log("\nScenario : Login as different user. Radius value should be same",true);
//		s.clickLogout();
//		s.clickUserlogout(enumber);
//		
//		Reporter.log("\nLogin as KEERTHI",true);
//		l.setEnumber("111");
//		l.setDate("01");
//		l.setMonth("10");
//		l.setYear("2017");
//		l.clickSubmit();	
//		
//		Thread.sleep(3000);
//		s.clickSettings();
//		Thread.sleep(3000);
//		s.verifyRadius();
//	}
//	
//	

//
//

//	
//	@Test(priority=8)
//	public void testStore()
//	{
//		Store s=new Store(driver);
//		s.clickStoreTab();
//		s.clickAddStore();
//		s.enterStoreNumber(StoreNumber);
//		s.enterStoreName(StoreName);
//		s.enterStoreAddress(StoreAddress);
//		s.enterStoreRadius(StoreRadius);
//		s.enterStoreLatitude(StoreLatitude);
//		s.enterStoreLongitude(StoreLongitude);
//		s.clickStoreSubmit();
//	}

