package generic;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;

public abstract class BaseTestCardinal implements AutoConstantCardinal
{
	public WebDriver driver;
	@BeforeSuite
	public void openApplication() throws InterruptedException
	{
	//	System.setProperty(GECKO_KEY, GECKO_VALUE);
		System.setProperty(CHROME_KEY, CHROME_VALUE);
		
		driver=new ChromeDriver();
		driver.get("https://cardinal-newui.2docstore.com/");
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("window.resizeTo(1366, 768);");
	//	driver.manage().window().maximize();
	//	driver.get("https://cardinal-newui.2docstore.com/#/report");
	//	Thread.sleep(10000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		
	}
	
//	@AfterSuite
//	public void closeApplication()
//	{
//		driver.quit();
//	}


}